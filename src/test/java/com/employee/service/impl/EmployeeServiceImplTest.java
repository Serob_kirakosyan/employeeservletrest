package com.employee.service.impl;

import com.employee.DataManager;
import com.employee.dao.EmployeeDao;
import com.employee.dto.EmployeeResponseDto;
import com.employee.entity.Employee;
import com.employee.service.EmployeeService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class EmployeeServiceImplTest {

    @Mock
    private EmployeeDao employeeDao;

    @InjectMocks
    private EmployeeService employeeService = EmployeeServiceImpl.getInstance();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void save() {
    }

    @Test
    public void getByUserNameAndPassword() {
        // GIVEN
        Employee employee = DataManager.getEmployeeWithId();
        when(employeeDao.getByUsernameAndPassword(any(), any())).thenReturn(employee);

        // WHEN
        EmployeeResponseDto responseDto = employeeService.getByUserNameAndPassword(employee.getUsername(), employee.getPassword());

        // THEN
        assertNotNull(responseDto);
        assertEquals(responseDto.getId(), employee.getId());
    }

    @Test
    public void getAll() {
    }

    @Test
    public void getById() {
    }

    @Test
    public void updateById() {
    }

    @Test
    public void removingById() {
    }
}