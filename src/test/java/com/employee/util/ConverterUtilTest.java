package com.employee.util;

import com.employee.DataManager;
import com.employee.dto.EmployeeRequestDto;
import com.employee.dto.EmployeeResponseDto;
import com.employee.entity.Employee;
import org.junit.Assert;
import org.junit.Test;

public class ConverterUtilTest {

    @Test
    public void convertFromEmployeeToEmployeeResponseDtoTest() {
        Employee employee = DataManager.getEmployeeWithId();
        EmployeeResponseDto expected = DataManager.getEmployeeResponseDto();
        EmployeeResponseDto actual = ConverterUtil.convertFromEmployeeToEmployeeResponseDto(employee);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void convertFromEmployeeRequestDtoToEmploy() {
        EmployeeRequestDto employeeRequestDto = DataManager.getEmployeeRequestDto();
        Employee expected = DataManager.getEmployee();
        Employee actual = ConverterUtil.convertFromEmployeeRequestDtoToEmploy(employeeRequestDto);
        Assert.assertEquals(expected, actual);
    }
}