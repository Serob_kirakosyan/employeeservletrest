package com.employee;

import com.employee.dto.EmployeeRequestDto;
import com.employee.dto.EmployeeResponseDto;
import com.employee.entity.Employee;

public class DataManager {

    private static final Employee employee;
    private static final Employee employeeWithId;
    private static final Employee employeeToUpdate;
    private static final EmployeeRequestDto employeeRequestDto;
    private static final EmployeeResponseDto employeeResponseDto;

    static {
        employee = new Employee("John", "Doe", 28, 300000, "John", "1234");
        employeeWithId = new Employee(1, "John", "Doe", 28, 300000, "John", "1234");
        employeeToUpdate = new Employee("John", "Smith", 32, 30000, "Smith", "1234");
        employeeResponseDto = new EmployeeResponseDto(1, "John", "Doe", 28, 300000);
        employeeRequestDto = new EmployeeRequestDto("John", "Doe", 28, 300000, "John", "1234");
    }

    public static Employee getEmployee() {
        return employee;
    }

    public static Employee getEmployee(int id) {
        employee.setId(id);
        return employee;
    }

    public static Employee getEmployeeWithId() {
        return employeeWithId;
    }

    public static Employee getEmployeeToUpdate(int id) {
        employeeToUpdate.setId(id);
        return employeeToUpdate;
    }

    public static Employee getEmployeeToUpdate() {
        return employeeToUpdate;
    }

    public static EmployeeResponseDto getEmployeeResponseDto() {
        return employeeResponseDto;
    }

    public static EmployeeRequestDto getEmployeeRequestDto() {
        return employeeRequestDto;
    }
}
