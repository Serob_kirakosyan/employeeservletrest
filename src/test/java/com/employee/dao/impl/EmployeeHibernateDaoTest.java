package com.employee.dao.impl;

import com.employee.DataManager;
import com.employee.entity.Employee;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class EmployeeHibernateDaoTest {

    private EmployeeHibernateDao instance = EmployeeHibernateDao.getInstance();

    @Before
    public void init() {
        // invoked before each test
    }

    @After
    public void destroy() {
        // invoked after each test
    }

    @Test
    public void save() {
        Employee employee = DataManager.getEmployee();
        Employee savedEmployee = instance.save(employee);
        assertNotEquals(savedEmployee.getId(), 0);
    }

    @Test
    public void getByUsernameAndPassword() {
        Employee employee = DataManager.getEmployee();
        employee.setUsername("test");
        employee.setPassword("test");
        instance.save(employee);
        Employee getEmployee = instance.getByUsernameAndPassword(employee.getUsername(), employee.getPassword());
        assertNotNull(getEmployee);
    }

    @Test
    public void getAll() {
        Employee employee = DataManager.getEmployee();
        instance.save(employee);
        List<Employee> employees = instance.getAll();
        assertNotNull(employees);
    }

    @Test
    public void getById() {
        Employee employee = DataManager.getEmployee();
        instance.save(employee);
        int id = 1;
        Employee getEmployee = instance.getById(id);
        assertNotNull(getEmployee);
    }

    @Test
    public void updateById() {
        Employee employee = DataManager.getEmployee();
        Employee savedEmployee = instance.save(employee);
        int id = savedEmployee.getId();
        Employee updateEmployee = DataManager.getEmployeeToUpdate();
        instance.updateById(employee, id);
        assertEquals(updateEmployee.getId(), 0);
    }

    @Test
    public void removeById() {
        Employee employee = DataManager.getEmployee();
        Employee savedEmployee = instance.save(employee);
        int id = savedEmployee.getId();
        instance.removeById(id);
        Employee deleteEmployee = instance.getById(id);
        assertNull(deleteEmployee);
    }
}