package com.employee.controller;

import com.employee.dao.impl.EmployeeHibernateDao;
import com.employee.dto.EmployeeRequestDto;
import com.employee.dto.EmployeeResponseDto;
import com.employee.dto.ResponseDto;
import com.employee.exception.ServiceException;
import com.employee.service.SignUpService;
import com.employee.service.impl.SignUpServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class SignUpServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(EmployeeHibernateDao.class);

    private SignUpService signUpService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init();

        signUpService = SignUpServiceImpl.getInstance();
    }

    /**
     * update employee entity in database by id
     *
     * @param request  HttpServletRequest request
     * @param response HttpServletResponse response
     * @throws IOException by SQL
     */

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        ResponseDto<EmployeeResponseDto> resp;
        EmployeeRequestDto employeeRequestDto;
        EmployeeResponseDto employeeResponseDto = null;
        String message;
        String body = null;
        try {
            body = getBody(request);
        } catch (IOException e) {
            LOG.error("IOException by reader");
        }
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            employeeRequestDto = objectMapper.readValue(body, EmployeeRequestDto.class);
            employeeResponseDto = signUpService.save(employeeRequestDto);
            if (employeeResponseDto != null) {
                response.setStatus(HttpServletResponse.SC_OK);
                message = "OK";
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                message = "Cannot create employee is not in good format";
            }
        } catch (JsonProcessingException | ServiceException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            message = "Not valid JSON provided";
        }
        response.setContentType("application/json");
        resp = new ResponseDto<>(employeeResponseDto, message);
        try {
            output(objectMapper.writeValueAsString(resp), response);
        } catch (IOException e) {
            LOG.error("IOException by writer");
        }
    }

    private String getBody(HttpServletRequest request) throws IOException {
        StringBuffer stringBuffer = new StringBuffer();
        String line = null;

        BufferedReader reader = request.getReader();
        while ((line = reader.readLine()) != null) {
            stringBuffer.append(line);
        }
        return stringBuffer.toString();
    }

    private void output(String message, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();
        writer.println(message);
        writer.flush();
    }
}
