package com.employee.controller;

import com.employee.dto.EmployeeResponseDto;
import com.employee.dto.ResponseDto;
import com.employee.exception.ServiceException;
import com.employee.service.SignInService;
import com.employee.service.impl.SignInServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class SignInServlet extends HttpServlet {

    private SignInService signInService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init();

        signInService = SignInServiceImpl.getInstance();
    }

    /**
     * get employee entity in database by username and password
     *
     * @param request  HttpServletRequest request
     * @param response HttpServletResponse response
     * @throws IOException by Json
     */

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ResponseDto<EmployeeResponseDto> resp;
        EmployeeResponseDto employeeResponseDto = null;
        String message;
        String body = getBody(request);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String username = objectMapper.readTree(body).findPath("username").asText();
            String password = objectMapper.readTree(body).findPath("password").asText();
            employeeResponseDto = signInService.getByUserNameAndPassword(username, password);
            if (employeeResponseDto != null) {
                response.setStatus(HttpServletResponse.SC_OK);
                message = "OK";
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                message = "Username or password is not in good format";
            }
        } catch (JsonProcessingException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            message = "Not valid JSON provided";
        } catch (ServiceException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            message = e.getMessage();
        }
        response.setContentType("application/json");
        resp = new ResponseDto<>(employeeResponseDto, message);
        output(objectMapper.writeValueAsString(resp), response);
    }

    private String getBody(HttpServletRequest request) throws IOException {
        StringBuffer stringBuffer = new StringBuffer();
        String line = null;

        BufferedReader reader = request.getReader();
        while ((line = reader.readLine()) != null) {
            stringBuffer.append(line);
        }
        return stringBuffer.toString();
    }

    private void output(String message, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();
        writer.println(message);
        writer.flush();
    }
}
