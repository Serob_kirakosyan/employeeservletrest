package com.employee.controller;

import com.employee.dao.impl.DepartmentDao;
import com.employee.dao.impl.EmployeeHibernateDao;
import com.employee.dto.EmployeeRequestDto;
import com.employee.dto.EmployeeResponseDto;
import com.employee.dto.ResponseDto;
import com.employee.entity.Department;
import com.employee.exception.ServiceException;
import com.employee.service.EmployeeService;
import com.employee.service.impl.EmployeeServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class EmployeeServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(EmployeeHibernateDao.class);
    EmployeeService employeeService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init();

        employeeService = EmployeeServiceImpl.getInstance();
    }

    /**
     * get employee entity
     *
     * @param request  HttpServletRequest request
     * @param response HttpServletResponse response
     * @throws IOException by JDBC
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        EmployeeResponseDto employeeResponseDto = null;
//        List<EmployeeResponseDto> employeeResponseDtoList = null;
//        String message;
//        ObjectMapper objectMapper = new ObjectMapper();
//
//
//        String[] split = request.getRequestURI().split("/");
//        if (split.length > 3) {
//            ResponseDto<EmployeeResponseDto> resp;
//            int id;
//            try {
//                id = Integer.parseInt(split[3]);
//                employeeResponseDto = employeeService.getById(id);
//                response.setStatus(HttpServletResponse.SC_OK);
//                message = "OK";
//            } catch (NumberFormatException e) {
//                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//                message = "That ID is not in good format";
//                LOG.error("Exception occurred when get employee by id");
//            } catch (ServiceException e) {
//                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//                message = e.getMessage();
//                LOG.error("Exception occurred when get employee by id " + message);
//            }
//
//            response.setContentType("application/json");
//            resp = new ResponseDto<>(employeeResponseDto, message);
//            output(objectMapper.writeValueAsString(resp), response);
//        } else {
//            ResponseDto<List<EmployeeResponseDto>> resp;
//            try {
//                employeeResponseDtoList = employeeService.getAll();
//                response.setStatus(HttpServletResponse.SC_OK);
//                message = "OK";
//            } catch (ServiceException e) {
//                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//                message = e.getMessage();
//                LOG.error("Exception occurred when get employees");
//            }
        ResponseDto<List<Department>> resp;
        DepartmentDao departmentDao = DepartmentDao.getInstance();
            response.setContentType("application/json");
            resp = new ResponseDto<>(departmentDao.getAll(), "message");
        ObjectMapper objectMapper = new ObjectMapper();
            output(objectMapper.writeValueAsString(resp), response);
     //   }


    }

    /**
     * update employee entity by id
     *
     * @param request  HttpServletRequest request
     * @param response HttpServletResponse response
     * @throws IOException by id number format
     */

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws
            IOException {
        ResponseDto<EmployeeResponseDto> resp;
        EmployeeResponseDto employeeResponseDto;
        employeeResponseDto = null;
        EmployeeRequestDto employeeRequestDto;
        String message;
        String body = getBody(request);
        ObjectMapper objectMapper = new ObjectMapper();
        try {

            int id = Integer.parseInt(request.getRequestURI().split("/")[3]);
            employeeRequestDto = objectMapper.readValue(body, EmployeeRequestDto.class);
            employeeResponseDto = employeeService.updateById(employeeRequestDto, id);
            response.setStatus(HttpServletResponse.SC_OK);
            message = "OK";
        } catch (NumberFormatException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            message = "ID is not in good format";
            LOG.error("Exception occurred when update employee by id " + message);
        } catch (ServiceException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            message = e.getMessage();
            LOG.error("Exception occurred when update employee by id " + message);
        }
        response.setContentType("application/json");
        resp = new ResponseDto<>(employeeResponseDto, message);
        output(objectMapper.writeValueAsString(resp), response);
    }

    /**
     * delete employee entity by id
     *
     * @param request  HttpServletRequest request
     * @param response HttpServletResponse response
     * @throws IOException by id number format
     */

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws
            IOException {
//        String message;
//        String body = getBody(request);
//        ObjectMapper objectMapper = new ObjectMapper();
//        try {
//            Integer id = Integer.parseInt(request.getRequestURI().split("/")[3]);
//            employeeService.removingById(id);
//            response.setStatus(HttpServletResponse.SC_OK);
//            message = "your employee removed";
//
//        } catch (ServiceException e) {
//            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//            message = "That is not in good format";
//        }
        ObjectMapper objectMapper = new ObjectMapper();
        DepartmentDao departmentDao = DepartmentDao.getInstance();
        response.setContentType("application/json");
        Integer id = Integer.parseInt(request.getRequestURI().split("/")[3]);
        departmentDao.removeById(id);

    }

    /**
     * get body toString
     *
     * @param request HttpServletRequest request
     * @return String body
     * @throws IOException by file reader
     */

    private String getBody(HttpServletRequest request) throws IOException {
        StringBuffer stringBuffer = new StringBuffer();
        String line;

        BufferedReader reader = request.getReader();
        while ((line = reader.readLine()) != null) {
            stringBuffer.append(line);
        }
        return stringBuffer.toString();
    }

    private void output(String message, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();
        writer.println(message);
        writer.flush();
    }
}
