package com.employee.dao;

import com.employee.entity.Employee;
import com.employee.exception.EmployeeException;

public interface EmployeeDao extends BaseDao<Employee> {

    /**
     * Get entity in database.
     *
     * @param username employee username
     * @param password employee password
     * @return {@link Employee}
     * @throws EmployeeException when Employee not found.
     */
    Employee getByUsernameAndPassword(String username, String password) throws EmployeeException;
}
