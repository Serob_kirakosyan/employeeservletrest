package com.employee.dao.impl;

import com.employee.dao.EmployeeDao;
import com.employee.entity.Employee;
import com.employee.exception.DaoException;
import com.employee.manager.HibernateManager;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class EmployeeHibernateDao implements EmployeeDao {
    private static final Logger LOG = LoggerFactory.getLogger(EmployeeHibernateDao.class);

    private static EmployeeHibernateDao instance = new EmployeeHibernateDao();

    public static EmployeeHibernateDao getInstance() {
        return instance;
    }

    @Override
    public Employee save(Employee employee) throws DaoException {
        final Session session = HibernateManager.getSessionFactory().openSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            employee.setId((Integer) session.save(employee));
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            LOG.error("Exception occurred when saving employee " + e.getMessage());
            throw new DaoException("Exception occurred when saving employee ");
        }
        return employee;
    }

    @Override
    public Employee getByUsernameAndPassword(String username, String password) throws DaoException {
        final Session session = HibernateManager.getSessionFactory().openSession();
        Transaction tx = null;
        Employee employee = null;
        try {
            tx = session.beginTransaction();
            Query<Employee> query = session.createQuery(
                    "FROM Employee where username = :username and password = :password", Employee.class);
            query.setParameter("username", username);
            query.setParameter("password", password);
            employee = query.getSingleResult();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            LOG.error("Exception occurred when get employee" + e.getMessage());
            throw new DaoException("Exception occurred when get employee");
        } finally {
            session.close();
        }
        return employee;
    }

    @Override
    public List<Employee> getAll() throws DaoException {
        final Session session = HibernateManager.getSessionFactory().openSession();
        Transaction tx = null;
        List<Employee> employee;

        try {
            tx = session.beginTransaction();
            employee = session.createQuery("FROM Employee", Employee.class).list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            LOG.error("Exception occurred when get employees" + e.getMessage());
            throw new DaoException("Exception occurred when get employees");
        } finally {
            session.close();
        }
        return employee;
    }

    @Override
    public Employee getById(final int id) throws DaoException {
        final Session session = HibernateManager.getSessionFactory().openSession();
        Employee employee = null;
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            employee = session.get(Employee.class, id);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
                LOG.error("Exception occurred when get employee by id" + e.getMessage());
                throw new DaoException(e.getMessage());
            }
        } finally {
            session.close();
        }
        return employee;
    }

    @Override
    public void updateById(Employee employee, final int id) throws DaoException {
        final Session session = HibernateManager.getSessionFactory().openSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            employee.setId(id);
            session.merge(employee);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            LOG.error("Exception occurred when update employee by id" + e.getMessage());
            throw new DaoException(e.getMessage());
        } finally {
            session.close();
        }
    }

    @Override
    public void removeById(final int id) throws DaoException {
        final Session session = HibernateManager.getSessionFactory().openSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            Employee employee = session.get(Employee.class, id);
            session.delete(employee);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            LOG.error("Exception occurred when delete employee" + e.getMessage());
            throw new DaoException("Exception occurred when delete employees by id");
        } finally {
            session.close();
        }
    }
}
