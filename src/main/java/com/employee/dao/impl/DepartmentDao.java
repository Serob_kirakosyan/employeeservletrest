package com.employee.dao.impl;

import com.employee.dao.BaseDao;
import com.employee.entity.Department;
import com.employee.exception.DaoException;
import com.employee.manager.HibernateManager;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class DepartmentDao implements BaseDao<Department> {
    private static final Logger LOG = LoggerFactory.getLogger(EmployeeHibernateDao.class);

    private static DepartmentDao instance = new DepartmentDao();

    public static DepartmentDao getInstance() {
        return instance;
    }

    @Override
    public Department save(Department department) {
        Session session = HibernateManager.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            department.setId((Integer) session.save(department));
            transaction.commit();
        } catch (
                HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
                LOG.error("Exception occurred when saving department" + e.getMessage());
                throw new DaoException("Exception occurred when saving department");
            }
        } finally {
            session.close();
        }
        return department;
    }

    @Override
    public List<Department> getAll() throws DaoException {
        final Session session = HibernateManager.getSessionFactory().openSession();
        Transaction transaction = null;
        List<Department> departments;

        try {
            transaction = session.beginTransaction();
            departments = session.createQuery("FROM Department", Department.class).list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            LOG.error("Exception occurred when get departments" + e.getMessage());
            throw new DaoException("Exception occurred when get departments");
        } finally {
            session.close();
        }
        return departments;
    }

    @Override
    public Department getById(final int id) throws DaoException {
        final Session session = HibernateManager.getSessionFactory().openSession();
        Department department = null;
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            department = session.get(Department.class, id);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null)
                transaction.rollback();
            LOG.error("Exception occurred when get department by id" + e.getMessage());
            throw new DaoException(e.getMessage());
        } finally {
            session.close();
        }
        return department;
    }

    @Override
    public void updateById(Department department, final int id) {
        final Session session = HibernateManager.getSessionFactory().openSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            department.setId(id);
            session.merge(department);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            LOG.error("Exception occurred when update department by id" + e.getMessage());
            throw new DaoException(e.getMessage());
        } finally {
            session.close();
        }
    }

    @Override
    public void removeById(final int id) {
        final Session session = HibernateManager.getSessionFactory().openSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            Department department = session.get(Department.class, id);
            session.delete(department);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null)
                transaction.rollback();
            LOG.error("Exception occurred when delete department" + e.getMessage());
            throw new DaoException("Exception occurred when delete department by id");
        } finally {
            session.close();
        }
    }
}
