package com.employee.dao.impl;

import com.employee.dao.BaseDao;
import com.employee.dao.EmployeeDao;
import com.employee.entity.Employee;
import com.employee.exception.DaoException;
import com.employee.manager.ConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EmployeeJDBCDao implements EmployeeDao {

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeJDBCDao.class);

    private static BaseDao instance = new EmployeeJDBCDao();

    public static BaseDao getInstance() {
        return instance;
    }


    @Override
    public Employee save(final Employee employee) throws DaoException {
        Connection connection = null;
        int id = 0;
        try {

            connection = ConnectionManager.getConnection();
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            connection.setAutoCommit(false);

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO employs(firstname,lastname,age,salary,username,password) VALUES(?,?,?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setString(1, employee.getFirstName());
            preparedStatement.setString(2, employee.getLastName());
            preparedStatement.setInt(3, employee.getAge());
            preparedStatement.setInt(4, employee.getSalary());
            preparedStatement.setString(5, employee.getUsername());
            preparedStatement.setString(6, employee.getPassword());

            preparedStatement.executeUpdate();
            final ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet != null && resultSet.next()) {
                id = resultSet.getInt(1);
            }
            connection.commit();


        } catch (SQLException e) {
            ConnectionManager.rollback(connection);
            LOG.error("Exception occurred when saving employee " + e.getMessage());
            throw new DaoException(e.getMessage());
        } finally {
            ConnectionManager.closeConnection(connection);
        }
        employee.setId(id);
        return employee;
    }

   @Override
    public Employee getByUsernameAndPassword(String username, String password) {
        Connection connection = null;
        try {

            connection = ConnectionManager.getConnection();
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            connection.setAutoCommit(false);

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT * FROM employs WHERE username = ? and password = ?");
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                return parseResultSet(resultSet);
            }
            connection.commit();
        } catch (SQLException e) {
            ConnectionManager.rollback(connection);
            LOG.error("Exception occurred when get employee" + e.getMessage());
        } finally {
            ConnectionManager.closeConnection(connection);
        }

        return null;
    }

    public List<Employee> getAll() {
        Connection connection = null;
        try {
            connection = ConnectionManager.getConnection();
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            connection.setAutoCommit(false);

            final PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM employs");

            final ResultSet resultSet = preparedStatement.executeQuery();

            List<Employee> employees = new ArrayList<Employee>();
            while (resultSet.next()) {
                Employee emp = parseResultSet(resultSet);
                employees.add(emp);
            }
            return employees;

        } catch (SQLException e) {
            ConnectionManager.rollback(connection);
            LOG.error("Exception occurred when get employees" + e.getMessage());
        } finally {
            ConnectionManager.closeConnection(connection);
        }

        return Collections.emptyList();
    }

    public Employee getById(final int Id) {
        Connection connection = null;
        try {

            connection = ConnectionManager.getConnection();
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            connection.setAutoCommit(false);

            final PreparedStatement preparedStatement = connection.prepareStatement("Select * from employs where id = ?");
            preparedStatement.setInt(1, Id);
            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                return parseResultSet(resultSet);
            }
            connection.commit();
        } catch (SQLException e) {
            ConnectionManager.rollback(connection);
            LOG.error("Exception occurred when get employee by id" + e.getMessage());
        } finally {
            ConnectionManager.closeConnection(connection);
        }

        return null;
    }

    public void updateById(Employee employee, final int id) {
        Connection connection = null;
        try {
            connection = ConnectionManager.getConnection();
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            connection.setAutoCommit(false);

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    "update employs set firstName = ?,lastName = ?,age = ?,salary = ?,password = ?,username = ?" +
                            "  where id = ?");

            preparedStatement.setString(1, employee.getFirstName());
            preparedStatement.setString(2, employee.getLastName());
            preparedStatement.setInt(3, employee.getAge());
            preparedStatement.setInt(4, employee.getSalary());
            preparedStatement.setString(5, employee.getPassword());
            preparedStatement.setString(6, employee.getUsername());
            preparedStatement.setInt(7, id);
            preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            ConnectionManager.rollback(connection);
            LOG.error("Exception occurred when update employee by id" + e.getMessage());
        } finally {
            ConnectionManager.closeConnection(connection);
        }
    }

    public void removeById(final int id) {
        Connection connection = null;
        try {
            connection = ConnectionManager.getConnection();
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            connection.setAutoCommit(false);

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    "delete from my_schema.employs where id = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            ConnectionManager.rollback(connection);
            LOG.error("Exception occurred when delete employee" + e.getMessage());
        } finally {
            ConnectionManager.closeConnection(connection);
        }
    }

    /**
     * @param resultSet for employee
     * @return {@link Employee}
     * @throws SQLException
     */
    private Employee parseResultSet(ResultSet resultSet) throws SQLException {
        Employee employee = new Employee();
        employee.setId(resultSet.getInt("id"));
        employee.setFirstName(resultSet.getString("firstName"));
        employee.setLastName(resultSet.getString("lastName"));
        employee.setAge(resultSet.getInt("age"));
        employee.setSalary(resultSet.getInt("salary"));
        employee.setUsername(resultSet.getString("username"));
        employee.setPassword(resultSet.getString("password"));
        return employee;
    }
}
