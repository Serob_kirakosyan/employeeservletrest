package com.employee.dao;

import com.employee.entity.Employee;
import com.employee.exception.DaoException;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Serob Kirakosyan
 */
public interface BaseDao<T> {

    /**
     * Save entity in database.
     *
     * @param t entity
     * @return  save entity
     * @throws DaoException by JDBC
     */
    T save(T t) throws DaoException;

    /**
     * Get entity list database.
     *
     * @return  list get entity
     * @throws  DaoException by JDBC
     */
    List<T> getAll() throws DaoException;

    /**
     * Get entity in database by id.
     *
     * @param Id entity Id
     * @return  get entity
     */
    T getById(int Id) throws DaoException;

    /**
     * Update entity in database by id.
     *
     * @param t entity
     * @param id  entity id
     * @throws DaoException by JDBC
     */
    void updateById(T t, int id) throws DaoException;

    /**
     * Delete entity in database by id.
     *
     * @param id entity id
     * @throws DaoException by JDBC
     */
    void removeById(int id) throws DaoException;
}
