package com.employee.util;

import com.employee.dto.EmployeeRequestDto;
import com.employee.dto.EmployeeResponseDto;
import com.employee.entity.Employee;

/**
 * @author Serob Kirakosyan
 */
public class ConverterUtil {
    /**
     * convert employee to employeeResponseDto.
     *
     * @param employee {@link Employee}
     * @return employeeResponseDto {@link EmployeeResponseDto}
     */
    public static EmployeeResponseDto convertFromEmployeeToEmployeeResponseDto(Employee employee) {
        EmployeeResponseDto employeeResponseDto = new EmployeeResponseDto();
        employeeResponseDto.setId(employee.getId());
        employeeResponseDto.setAge(employee.getAge());
        employeeResponseDto.setFirstName(employee.getFirstName());
        employeeResponseDto.setLastName(employee.getLastName());
        employeeResponseDto.setSalary(employee.getSalary());
        return employeeResponseDto;
    }

    /**
     * convert employee to employeeRequestDto.
     *
     * @param employeeRequestDto {@link EmployeeRequestDto}
     * @return employee {@link Employee}
     */

    public static Employee convertFromEmployeeRequestDtoToEmploy(EmployeeRequestDto employeeRequestDto) {
        Employee employee = new Employee();
        employee.setFirstName(employeeRequestDto.getFirstName());
        employee.setLastName(employeeRequestDto.getLastName());
        employee.setAge(employeeRequestDto.getAge());
        employee.setSalary(employeeRequestDto.getSalary());
        employee.setUsername(employeeRequestDto.getUsername());
        employee.setPassword(employeeRequestDto.getPassword());
        return employee;
    }
}
