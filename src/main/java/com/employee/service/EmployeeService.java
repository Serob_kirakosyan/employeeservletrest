package com.employee.service;

import com.employee.dto.EmployeeRequestDto;
import com.employee.dto.EmployeeResponseDto;
import com.employee.exception.ServiceException;

import java.util.List;

/**
 * @author Serob Kirakosyan
 */
public interface EmployeeService {
    /**
     * Save employee entity in database.
     *
     * @param employeeRequestDto EmployeeRequestDto
     * @return {@link EmployeeResponseDto} save entity
     * @throws ServiceException business exception
     */
    EmployeeResponseDto save(EmployeeRequestDto employeeRequestDto) throws ServiceException;

    /**
     * Get employee entity in database.
     *
     * @param username employee username
     * @param password employee password
     * @return {@link EmployeeResponseDto} get entity
     * @throws ServiceException business exception
     */

    EmployeeResponseDto getByUserNameAndPassword(String username, String password) throws ServiceException;

    /**
     * Get employee entity list database.
     *
     * @return {@link EmployeeResponseDto} list get entity
     * @throws ServiceException business exception
     */

    List<EmployeeResponseDto> getAll() throws ServiceException;

    /**
     * Get employee entity in database by id.
     *
     * @param id employee id
     * @return {@link EmployeeResponseDto} get entity
     * @throws ServiceException business exception
     */

    EmployeeResponseDto getById(int id) throws ServiceException;

    /**
     * Update employee entity in database by id.
     *
     * @param employeeRequestDto {@link EmployeeRequestDto}
     * @param id                 employee id
     * @return {@link EmployeeResponseDto} update entity
     * @throws ServiceException business exception
     */

    EmployeeResponseDto updateById(EmployeeRequestDto employeeRequestDto, int id) throws ServiceException;

    /**
     * Delete employee in database by id.
     *
     * @param id employee id
     * @throws ServiceException business exception
     */

    void removingById(int id) throws ServiceException;
}
