package com.employee.service.impl;

import com.employee.dto.EmployeeRequestDto;
import com.employee.dto.EmployeeResponseDto;
import com.employee.exception.ServiceException;
import com.employee.service.EmployeeService;
import com.employee.service.SignUpService;

public class SignUpServiceImpl implements SignUpService {

    private static final SignUpServiceImpl instance = new SignUpServiceImpl();

    private final EmployeeService employeeService;

    private SignUpServiceImpl() {
        employeeService = EmployeeServiceImpl.getInstance();
    }

    public static SignUpServiceImpl getInstance() {
        return instance;
    }

    @Override
    public EmployeeResponseDto save(EmployeeRequestDto employeeRequestDto) throws ServiceException {
        return employeeService.save(employeeRequestDto);
    }
}
