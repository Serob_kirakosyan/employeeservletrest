package com.employee.service.impl;

import com.employee.dao.EmployeeDao;
import com.employee.dao.impl.EmployeeHibernateDao;
import com.employee.dto.EmployeeRequestDto;
import com.employee.dto.EmployeeResponseDto;
import com.employee.entity.Employee;
import com.employee.exception.DaoException;
import com.employee.exception.EmployeeNotFoundException;
import com.employee.exception.ServiceException;
import com.employee.service.EmployeeService;
import com.employee.util.ConverterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

public class EmployeeServiceImpl implements EmployeeService {
    private static final Logger LOG = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    private static EmployeeServiceImpl instance = new EmployeeServiceImpl();

    private EmployeeDao employeeDao;

    private EmployeeServiceImpl() {
        employeeDao = EmployeeHibernateDao.getInstance();
    }

    public static EmployeeServiceImpl getInstance() {
        return instance;
    }

    public EmployeeResponseDto save(EmployeeRequestDto employeeRequestDto) throws ServiceException {
        LOG.info("Save employee : save()");
        Employee employee = ConverterUtil.convertFromEmployeeRequestDtoToEmploy(employeeRequestDto);
        try {
            employeeDao.save(employee);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
        return ConverterUtil.convertFromEmployeeToEmployeeResponseDto(employee);
    }


    public EmployeeResponseDto getByUserNameAndPassword(String username, String password) throws ServiceException {
        LOG.info("Get employee by username and password : getByUserNameAndPassword()");
        Employee employee;
        try {
            employee = employeeDao.getByUsernameAndPassword(username, password);
        } catch (DaoException e) {
            LOG.error("Exception occurred when get employee");
            throw new ServiceException(e.getMessage());
        }
        if (employee != null) {
            return ConverterUtil.convertFromEmployeeToEmployeeResponseDto(employee);
        } else {
            return null;
        }
    }

    public List<EmployeeResponseDto> getAll() throws ServiceException {
        LOG.info("Get all employees : getAll()");
        List<Employee> employees = null;
        try {
            employees = employeeDao.getAll();
        } catch (DaoException e) {
            LOG.error(e.getMessage() + " : getAll()");
            throw new ServiceException(e.getMessage());
        }
        return employees.stream().map(ConverterUtil::convertFromEmployeeToEmployeeResponseDto).collect(Collectors.toList());
    }

    public EmployeeResponseDto getById(int id) throws ServiceException {
        LOG.info("Get employee by id : getById()");
        Employee employee;
        try {
            employee = (Employee) employeeDao.getById(id);
        } catch (DaoException e) {
            LOG.error("Exception occurred when get employee by id" + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
        if (employee != null) {
            return ConverterUtil.convertFromEmployeeToEmployeeResponseDto(employee);
        } else {
            throw new EmployeeNotFoundException("Employee not found");
        }
    }

    public EmployeeResponseDto updateById(EmployeeRequestDto employeeRequestDto, int id) throws ServiceException {
        LOG.info("Update employee : updateById()");
        Employee employee = ConverterUtil.convertFromEmployeeRequestDtoToEmploy(employeeRequestDto);
        try {
            employeeDao.updateById(employee, id);
        } catch (DaoException e) {
            LOG.error("Exception occurred when update employee by id" + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
        return getById(id);

    }

    public void removingById(int id) throws ServiceException {
        LOG.info("delete employee by id : removingById()");
        try {
            employeeDao.removeById(id);

        } catch (DaoException e) {
            LOG.error("Exception occurred when delete employee" + e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }


}
