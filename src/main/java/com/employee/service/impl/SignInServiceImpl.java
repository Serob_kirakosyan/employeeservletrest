package com.employee.service.impl;

import com.employee.dto.EmployeeResponseDto;
import com.employee.exception.ServiceException;
import com.employee.service.EmployeeService;
import com.employee.service.SignInService;

public class SignInServiceImpl implements SignInService {

    private static final SignInServiceImpl instance = new SignInServiceImpl();

    private final EmployeeService employeeService;

    private SignInServiceImpl() {
        employeeService = EmployeeServiceImpl.getInstance();
    }

    public static SignInServiceImpl getInstance() {
        return instance;
    }

    @Override
    public EmployeeResponseDto getByUserNameAndPassword(String username, String password) throws ServiceException {
        return employeeService.getByUserNameAndPassword(username, password);
    }
}
