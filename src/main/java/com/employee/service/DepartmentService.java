package com.employee.service;

import com.employee.dao.BaseDao;
import com.employee.dao.impl.DepartmentDao;
import com.employee.entity.Department;
import com.employee.exception.DaoException;
import com.employee.exception.ServiceException;
import com.employee.util.ConverterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DepartmentService {
    private static final Logger LOG = LoggerFactory.getLogger(DepartmentService.class);

    private static DepartmentService instance = new DepartmentService();

    private BaseDao baseDao;

    private DepartmentService() {
        baseDao = DepartmentDao.getInstance();
    }

    public static DepartmentService getInstance() {
        return instance;
    }

    public Department save(Department department) throws ServiceException {
        LOG.info("Save department : save()");
        try {
            baseDao.save(department);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
        return department;
    }
}
