package com.employee.service;

import com.employee.dto.EmployeeResponseDto;
import com.employee.exception.ServiceException;

public interface SignInService {
    /**
     * Get employee entity in database.
     *
     * @param username employee username
     * @param password employee password
     * @return {@link EmployeeResponseDto} get entity
     * @throws ServiceException business exception
     */

    EmployeeResponseDto getByUserNameAndPassword(String username, String password) throws ServiceException;
}
