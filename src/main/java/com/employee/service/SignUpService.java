package com.employee.service;

import com.employee.dto.EmployeeRequestDto;
import com.employee.dto.EmployeeResponseDto;
import com.employee.exception.ServiceException;

public interface SignUpService {
    /**
     * Save employee entity in database.
     *
     * @param employeeRequestDto EmployeeRequestDto
     * @return {@link EmployeeResponseDto} save entity
     * @throws ServiceException business exception
     */
    EmployeeResponseDto save(EmployeeRequestDto employeeRequestDto) throws ServiceException;
}
