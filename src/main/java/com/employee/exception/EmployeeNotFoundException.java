package com.employee.exception;

public class EmployeeNotFoundException extends ServiceException {

    public EmployeeNotFoundException(String message) {
        super(message);
    }
}
