package com.employee.exception;

public class EmployeeException extends ServiceException{

    public EmployeeException(String message) {
        super(message);
    }
}
